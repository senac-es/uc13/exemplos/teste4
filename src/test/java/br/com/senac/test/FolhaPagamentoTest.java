/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.ex4.FolhaPagamento;
import br.com.senac.ex4.Produto;
import br.com.senac.ex4.Venda;
import br.com.senac.ex4.Vendedor;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class FolhaPagamentoTest {
    
    public FolhaPagamentoTest() {
    }
    
    @Test
    public void deveCalcularComissaoParaJose(){
        
        Vendedor vendedor = new Vendedor(1, "Jose");
        Produto produto = new Produto(1, "Sabao");
        
        
        Venda venda1 = new Venda(vendedor) ;
        venda1.adicionarItem(produto , 1 , 10 ) ;
        
        Venda venda2 = new Venda(vendedor);
        venda2.adicionarItem(produto, 1, 10);
        
        List<Venda> listaVendas = new ArrayList<>() ; 
        listaVendas.add(venda1);
        listaVendas.add(venda2);
        
        FolhaPagamento folhaPagamento = new FolhaPagamento() ; 
        folhaPagamento.calcularComissao(listaVendas) ; 
        double comissao = folhaPagamento.getComissaoVendedor(vendedor);
        
        assertEquals(1, comissao , 0.01);
        
        
        
        
        
        
    }
    
    
    @Test
    public void deveCalcularComissaoParaJoseEMiguel(){
        
        Vendedor jose = new Vendedor(1, "Jose");
        Vendedor miguel = new Vendedor(2, "Miguel");
        
        Produto produto = new Produto(1, "Sabao");
        
        
        Venda venda1 = new Venda(jose) ;
        venda1.adicionarItem(produto , 10 , 10 ) ;
        
        Venda venda2 = new Venda(miguel);
        venda2.adicionarItem(produto, 10, 20);
        
        List<Venda> listaVendas = new ArrayList<>() ; 
        listaVendas.add(venda1);
        listaVendas.add(venda2);
        
        FolhaPagamento folhaPagamento = new FolhaPagamento() ; 
        folhaPagamento.calcularComissao(listaVendas) ; 
        double comissaoJose = folhaPagamento.getComissaoVendedor(jose) ; 
        
        assertEquals(5.0, comissaoJose , 0.01);
        
        
        
        
        
        
    }
    
    
    
    
}
