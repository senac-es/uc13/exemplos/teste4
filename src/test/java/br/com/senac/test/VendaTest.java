
package br.com.senac.test;

import br.com.senac.ex4.Produto;
import br.com.senac.ex4.Venda;
import br.com.senac.ex4.Vendedor;
import org.junit.Test;
import static org.junit.Assert.*;


public class VendaTest {
    
    public VendaTest() {
    }
    
    
    @Test
    public void deveSomaTotalNota(){
        
        Venda venda = new Venda(new Vendedor(1, "Daniel"));
        Produto produto = new Produto(1, "Sabao");
        venda.adicionarItem(produto , 1 , 10 ) ; 
        
        double resultado = venda.getTotal() ; 
        
        
        assertEquals(resultado, 10 , 0.05);
        
    }
    
    
    
    
    
    
}
